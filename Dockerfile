FROM archlinux as base

RUN pacman-key --refresh-keys || true \
&& pacman -Syu --noconfirm \
&& rm -rf /var/cache/pacman/pkg/*

RUN pacman -Syuq --noconfirm git base-devel sudo \
&& rm -rf /var/cache/pacman/pkg/*

RUN echo "Defaults         lecture = never" > /etc/sudoers.d/privacy \
 && echo "%wheel ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/wheel \
 && useradd -m -G wheel -s /bin/bash builder

USER builder
WORKDIR /home/builder

RUN git clone https://aur.archlinux.org/yay.git \
 && cd yay \
 && makepkg -si --noconfirm \
 && cd .. \
 && rm -rf yay \
 && yay -Rsc --noconfirm go \
 && sudo rm -rf /var/cache/pacman/pkg/*

RUN sudo pacman -Syuq --noconfirm git base-devel sudo \
 && sudo rm -rf /var/cache/pacman/pkg/*

RUN sudo pacman -Syu --noconfirm && sudo rm -rf /var/cache/pacman/pkg/*
RUN sudo pacman -Syuq --noconfirm ccache && sudo rm -rf /var/cache/pacman/pkg/*
